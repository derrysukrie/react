import React from 'react';
import Header from './components/Header';
import { Button, Form, FormGroup, Label, Input, Col, Jumbotron } from 'reactstrap';
import axios from 'axios';

export default class AddProduct extends React.Component{
    constructor(props){
        super(props);
        this.state = { 
            title: '',
            categories: '',
            content: ''
        }
    }

    titleChangeHandle = event => {this.setState({title: event.target.value})};
    categoriesChangeHandle = event => {this.setState({categories: event.target.value})};
    contentChangedHandle = event => {this.setState({content: event.target.value})};

    submitHandle = event => {
        event.preventDefault();

        axios.post(`http://reduxblog.herokuapp.com/api/posts?key=derry`,
        {title: this.state.title, categories: this.state.categories, content: this.state.content})
        .then(response => {
            console.log(response);
            console.log(response.data);
            alert('Submited');
            this.props.history.push('/addproduct')
        })
    }


    render(){
        return(
            <div>
                <Header />
                <Jumbotron>
                    <h1 className="display-3">Hello, world!</h1>
                    <p className="lead">This is a simple hero unit, a simple Jumbotron-style component for calling extra attention to featured content or information.</p>
                    <hr className="my-2" />
                    <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
                    <p className="lead">
                    <Col md="6">
                            <Form onSubmit={this.submitHandle}>
                                <FormGroup>
                                    <Label for="exampleEmail">Nama Barang :</Label>
                                    <Input type="text" name="this.state.title" onChange={this.titleChangeHandle} id="exampleEmail" placeholder="with a placeholder"  bsSize="sm"/>
                                </FormGroup>
                                <FormGroup>
                                    <Label for="exampleEmail">Link Gambar :</Label>
                                    <Input type="text" name="this.state.categories" onChange={this.categoriesChangeHandle} id="exampleEmail" placeholder="with a placeholder" bsSize="sm" />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="exampleEmail">Detail :</Label>
                                    <Input type="textarea" name="this.state.content" onChange={this.contentChangedHandle} id="exampleEmail" placeholder="with a placeholder" bsSize="sm" />
                                </FormGroup>
                                <Button color="primary" type="submit" size="sm" block>Add</Button>
                            </Form>
                    </Col>  
                    </p>
                </Jumbotron>
            </div>
        );
    }
}