import React, { Component } from 'react'
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    Container} from 'reactstrap';
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import logo from '../logo.svg';


export default class Header extends Component{
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
          isOpen: false
        };
      }
      toggle() {
        this.setState({
          isOpen: !this.state.isOpen
        });
      }
     render(){
         return(
            <div>
                
                    <Navbar  light expand="md">
                    <Container>
                    <NavbarBrand><img src={logo} height="50" alt="logo"></img></NavbarBrand>
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav className="ml-auto" navbar>
                        <NavItem>
                            <NavLink><Link to='/'>Home</Link></NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink> <Link to="/addproduct">Add Product</Link></NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink> <Link to="/productlist">Product List</Link></NavLink>
                        </NavItem>
                        </Nav>
                    </Collapse>
                    </Container>
                    </Navbar> 
               
            </div>
         );
     }
}