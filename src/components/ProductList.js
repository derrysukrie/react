import React from 'react';
import { Card, CardImg, CardText, CardBody,
    CardTitle, Button, Container, Row,
    Col, Alert } from 'reactstrap';
import axios from 'axios';
import Header from './Header';

export default class ProductList extends React.Component{
    constructor(){
        super();
        this.toggle = this.toggle.bind(this);
        this.state = {
            toolTipopen: false,
            products: []
        }
    }

    toggle(){
        this.setState({
            toolTipopen: !this.state.toolTipopen
        })
    }

    componentDidMount(){
        axios.get(`http://reduxblog.herokuapp.com/api/posts?key=derry`)
        .then(response => {
            this.setState({                             // ambil data dari api, kemudian respon data tersebut dan masukan ke dalam products
                products: response.data
            })
        })
    }

    deleteHandle = productId => {
        const productupdate = [this.state.products];
        axios.delete(`http://reduxblog.herokuapp.com/api/posts/${productId}`)
        .then (upd => {
            console.log(upd);
            alert('ilang gan');
            this.setState ({products:productupdate});
            window.location.reload();
        })
        .catch(err => {
            console.log(err)
        })
    }

    render(){
        return(
            <div>    
            <Header />
            <Alert color="success">
                Nikmati diskon awal tahun!
            </Alert>
              <Container fluid>
                <Row className="d-flex">
                  {
                      this.state.products.map((product, index)=>
                      <Col key={index} xs="3">
                            <Card className="card1">
                                <CardImg top width="100%" src={product.categories}alt="Card image cap" />
                                <CardBody>
                                <CardTitle>Nama Product : {product.title}</CardTitle>
                                <CardText>{product.content}</CardText>
                                <Button onClick={(e) => this.deleteHandle(product.id)}>Delete</Button>
                                </CardBody>
                            </Card>
                        </Col>
                      )
                  }
                </Row>
                </Container>
                <Container fluid>
                    <hr></hr>
                </Container>
            </div>
        );
    }
}