import React from 'react';
import { Container, Row, Col, Button } from 'reactstrap';

export default class Body extends React.Component{
    render(){
        return(
            <div>
            <Container>
                <div className="badan">
                    <Row>
                        <Col xs="6">
                            <p className="text"> Selamat Datang!</p>
                            <p className="text2">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Fusce tempus, neque nec maximus aliquam, orci arcu blandit turpis, vitae molestie nibh purus a ante. 
                            Vestibulum a interdum velit. Sed faucibus eros eu enim pulvinar, ut aliquet leo auctor. In hac habitasse platea dictumst. Nam efficitur sem ut nisi accumsan volutpat
                            </p>
                            <Button color="primary" size="sm">Small Button</Button>
                        </Col>
                    </Row>
                </div>
            </Container>  
            </div>
        );
    }
}