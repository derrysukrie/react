import React from 'react'
import { BrowserRouter, Route } from 'react-router-dom';
import AddProduct from './AddProduct';
import Home from './Home';
import ProductList from './components/ProductList';

export default class AppRouter extends React.Component{
    render(){
        return(
            <div>
            <BrowserRouter>
                <div>
                    <Route exact path='/' component={Home}/>
                    <Route path='/addproduct' component={AddProduct}/>
                    <Route path='/productlist' component={ProductList}/>
                </div>
            </BrowserRouter> 
         
            </div>
        );
    }
}